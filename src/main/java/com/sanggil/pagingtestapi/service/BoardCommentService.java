package com.sanggil.pagingtestapi.service;

import com.sanggil.pagingtestapi.entity.BoardComment;
import com.sanggil.pagingtestapi.model.BoardCommentCreateRequest;
import com.sanggil.pagingtestapi.model.BoardCommentItem;
import com.sanggil.pagingtestapi.model.ListResult;
import com.sanggil.pagingtestapi.repository.BoardCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardCommentService {
    private final BoardCommentRepository boardCommentRepository;

    public void setComment(long boardId, BoardCommentCreateRequest createRequest) {
        BoardComment addData = new BoardComment.BoardCommentBuilder(boardId, createRequest).build();

        boardCommentRepository.save(addData);
    }

    public ListResult<BoardCommentItem> getComments(long boardId, int pageNum) {
        Page<BoardComment> originList = boardCommentRepository.findAllByBoardIdOrderByIdDesc(
                boardId, ListConvertService.getPageable(pageNum, 15));
        // Jpa를 이용함 Page<BoardComment>의 원본 originList는
        //boardCommentRepository에서 findAllByBoardIdOrderByIdDesc로 가져옴 boardId, PageNum를 줌 한 페이지당 15개
        List<BoardCommentItem> result = new LinkedList<>(); // 결과 리스트를 담을 새로운 리스트 생성

        for (BoardComment item : originList.getContent()) { // 원본리스트에서 원본을 차례대로 하나씩 던져준다.

            result.add(new BoardCommentItem.BoardCommentItemBuilder(item).build()); // 재가공해서 result에 넣는다.
        }

        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }
}
