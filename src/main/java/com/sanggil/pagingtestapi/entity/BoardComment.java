package com.sanggil.pagingtestapi.entity;

import com.sanggil.pagingtestapi.interfaces.CommonModelBuilder;
import com.sanggil.pagingtestapi.model.BoardCommentCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardComment {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 시퀀스")
    @Column(nullable = false)
    private Long boardId;

    @ApiModelProperty(notes = "작성자명")
    @Column(nullable = false, length = 20)
    private String writerName;

    @ApiModelProperty(notes = "댓글 내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private BoardComment(BoardCommentBuilder builder) {
        this.boardId = builder.boardId;
        this.writerName = builder.writerName;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
    }

    public static class BoardCommentBuilder implements CommonModelBuilder<BoardComment> {
        private final Long boardId;
        private final String writerName;
        private final String contents;
        private final LocalDateTime dateCreate;

        public BoardCommentBuilder(Long boardId, BoardCommentCreateRequest createRequest) {
            this.boardId = boardId;
            this.writerName = createRequest.getWriterName();
            this.contents = createRequest.getContents();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public BoardComment build() {
            return new BoardComment(this);
        }
    }
}
