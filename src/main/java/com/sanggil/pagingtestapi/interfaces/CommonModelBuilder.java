package com.sanggil.pagingtestapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
