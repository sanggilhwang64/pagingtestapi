package com.sanggil.pagingtestapi.model;

import com.sanggil.pagingtestapi.entity.BoardComment;
import com.sanggil.pagingtestapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardCommentItem {
    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long commentId;
    @ApiModelProperty(notes = "작성자명")
    private String writerName;
    @ApiModelProperty(notes = "댓글 내용")
    private String contents;
    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;

    private BoardCommentItem(BoardCommentItemBuilder builder) {
        this.commentId = builder.commentId;
        this.writerName = builder.writerName;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
    }
    public static class BoardCommentItemBuilder implements CommonModelBuilder<BoardCommentItem> {
        private final Long commentId;
        private final String writerName;
        private final String contents;
        private final LocalDateTime dateCreate;

        public BoardCommentItemBuilder(BoardComment boardComment) {
            this.commentId = boardComment.getId();
            this.writerName = boardComment.getWriterName();
            this.contents = boardComment.getContents();
            this.dateCreate = boardComment.getDateCreate();
        }

        @Override
        public BoardCommentItem build() {
            return new BoardCommentItem(this);
        }
    }
}
