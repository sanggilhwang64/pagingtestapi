package com.sanggil.pagingtestapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardCommentCreateRequest {

    @ApiModelProperty(notes = "작성자명 (2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String writerName;

    @ApiModelProperty(notes = "게시글 내용 (10~)", required = true)
    @NotNull
    @Length(min = 10)
    private String contents;


}
