package com.sanggil.pagingtestapi.repository;

import com.sanggil.pagingtestapi.entity.BoardComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardCommentRepository extends JpaRepository<BoardComment, Long> {
    Page<BoardComment> findAllByBoardIdOrderByIdDesc(long boardId, Pageable pageable);
    // List가 아닌 Page를 이용 검색조건은 BoardId가 일치하는 모든것 Id는 내림차순
}
