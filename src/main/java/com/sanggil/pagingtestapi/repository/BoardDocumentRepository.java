package com.sanggil.pagingtestapi.repository;


import com.sanggil.pagingtestapi.entity.BoardDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {
}
