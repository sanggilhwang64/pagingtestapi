package com.sanggil.pagingtestapi.controller;

import com.sanggil.pagingtestapi.model.BoardDocumentCreateRequest;
import com.sanggil.pagingtestapi.model.BoardDocumentItem;
import com.sanggil.pagingtestapi.model.CommonResult;
import com.sanggil.pagingtestapi.model.ListResult;
import com.sanggil.pagingtestapi.service.BoardDocumentService;
import com.sanggil.pagingtestapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-document")
public class BoardDocumentController {
    private final BoardDocumentService boardDocumentService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/new")
    public CommonResult setDocument(@RequestBody @Valid BoardDocumentCreateRequest createRequest) {
        boardDocumentService.setDocument(createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 리스트")
    @GetMapping("/all/page/{pageNum}")
    public ListResult<BoardDocumentItem> getDocuments(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardDocumentService.getDocuments(pageNum), true);
    }
}