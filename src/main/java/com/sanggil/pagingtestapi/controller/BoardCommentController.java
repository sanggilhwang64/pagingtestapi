package com.sanggil.pagingtestapi.controller;

import com.sanggil.pagingtestapi.model.BoardCommentCreateRequest;
import com.sanggil.pagingtestapi.model.BoardCommentItem;
import com.sanggil.pagingtestapi.model.CommonResult;
import com.sanggil.pagingtestapi.model.ListResult;
import com.sanggil.pagingtestapi.service.BoardCommentService;
import com.sanggil.pagingtestapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-comment")
public class BoardCommentController {
    private final BoardCommentService boardCommentService;

    @ApiOperation(value = "댓글 등록")
    @PostMapping("/board-id/{boardId}")
    public CommonResult setComment(@PathVariable long boardId, @RequestBody @Valid BoardCommentCreateRequest createRequest) {
        boardCommentService.setComment(boardId, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 리스트")
    @GetMapping("/all/board-id/{boardId}/page/{pageNum}")
    public ListResult<BoardCommentItem> getComments(@PathVariable long boardId, @PathVariable int pageNum) {
        return ResponseService.getListResult(boardCommentService.getComments(boardId, pageNum),true);
    }
}
